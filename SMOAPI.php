<?php

class SMOAPI
   {
      public $api_url = 'http://autosmo.com/api/v2'; // API URL

      public $api_key = '106d97ced42e7789a70a516c6541c601'; // Your API key

      public function order($data) { // add order
        $post = array_merge(array('key' => $this->api_key, 'action' => 'add'), $data);
        return ($this->connect($post));
      }

      public function status($order_id) { // get order status
        return json_decode($this->connect(array(
          'key' => $this->api_key,
          'action' => 'status',
          'id' => $order_id
        )));
      }

      public function services() { // get services
        return json_decode($this->connect(array(
          'key' => $this->api_key,
          'action' => 'services',
        )));
      }


      private function connect($post) {
        $_post = Array();
        if (is_array($post)) {
          foreach ($post as $name => $value) {
            $_post[] = $name.'='.urlencode($value);
          }
        }
        $ch = curl_init($this->api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if (is_array($post)) {
          curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        $result = curl_exec($ch);
        // var_dump($result);
        // if($result === false)
        // {

        //     echo 'Curl error: ' . curl_error($ch);
        // }
        // else{
        //   echo "NO ERROR";
        // }
        if (curl_errno($ch) != 0 && empty($result)) {
           
          $result = false;
        }
        curl_close($ch);
        return $result;
      }
   }
?>