<?php
 
require_once('function.php');
connectdb();
session_start();

$ttl = mysql_fetch_array(mysql_query("SELECT sitename FROM general_setting WHERE id='1'"));
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">


<title> <?php echo $ttl[0]; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="indx/css/bootstrap.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
<link href="indx/css/style.css" rel="stylesheet">




  <link rel="shortcut icon" href="images/fav.png" type="image/png">

<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body>
<header class="main__header">
  <div class="container">
    <nav class="navbar navbar-default"> 
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="contact">contact us</a></li>
          <li></li>
          <li></li>
          <li>FOLLOWS & LIKES</li>
          <li></li>
          <li></li>
          <li><a href="signin">Login</a></li>
          <li><a href="signup">Register</a></li>
        </ul>
      </div>
      <!-- /.navbar-collapse --> 
      
      <!-- Brand and toggle get grouped for better mobile display -->

    </nav>
  </div>
</header>



<section class="main__middle__container green_bg">


  <div class="container">
    <div class="row">
      <h2 class="text-center">Terms of Service</h2>
      <p class="text-center">By placing an order with Follows & Likes, you automatically accept all the below listed terms of services whether you read them or not:<br/>
Follows & Likes accepts payments in USD ($)<br/>
The default minimum payment accepted is $5<br/>
Once you make a deposit, you must use the funds at Follows & Likes. There are no cash refunds.<br/>
All payments are processed by Paypal. We do not store any financial information.<br/>
Follows & Likes DOES NOT guarantee full delivery within 24 hours. We make no guarantee for delivery time at all, 48 hours is a rough estimate. We will not be held responsible for loss of funds, negative reviews or you being banned on different sites for late delivery. If you are selling on a website that requires time sensitive results, use Follows & Likes at your own risk.<br/>
We DO NOT guarantee your new followers will interact with you, we simply guarantee you to get the followers you pay for.</p>

<h2 class="text-center">Refund Policy</h2>

<p class="text-center">No refunds will be made. After a deposit has been completed, there is no way to reverse it. You must use your balance for orders at Follows & Likes.<br/>
You agree that once you complete a payment, you will not file a dispute or a chargeback against Follows & Likes for any reason.<br/>
If you file a dispute or chargeback against Follows & Likes after a deposit, we reserve the right to terminate all future orders, ban you from our site. We also reserve the right to take away any followers or likes we delivered to your or your clients social media accounts .<br/>
Fraudulent activity such as using unauthorized or stolen credit cards will lead to termination of your account. There are no exceptions.</p>

<h2 class="text-center">Privacy Policy</h2>

<p class="text-center">This policy covers how we use your personal information. We take your privacy seriously and will take all measures to protect your personal information. Any personal information received will only be used to fulfill your order. We will not sell or redistribute your information to anyone. All information is encrypted and saved in secure servers.</p>

   </div>
  </div>

</section>


<footer>
  <div class="container">
    
    <p class="text-center">&copy; Copyright Follows & Likes. All Rights Reserved.</p>
  </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script type="text/javascript" src="indx/js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="indx/js/bootstrap.min.js"></script> 
<script type="text/javascript">

$('.carousel').carousel({
  interval: 3500, // in milliseconds
  pause: 'none' // set to 'true' to pause slider on mouse hover
})
</script>
</body>

</html>
