<?php
require_once('function.php');
connectdb();
session_start();

if (is_user()) {
	redirect("$baseurl/dashboard");
}
?>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- _new working -->
<link rel="stylesheet" href="extra/css/style.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


  <link rel="shortcut icon" href="extra/images/logo.png" type="image/png">
  <!-- /_new working-->

  <title>New Member</title>

  <link href="<?php echo $baseurl; ?>/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>
<!-- _new working -->
<style>
.data-container>h1,h5{color:white;}
.navbar-default {
    background-color:transparent;
    border-color: white;
}
.signinpanel {
    width: 780px;
    margin: 0% auto 3% auto;
}
.btn{background:black;}
</style>
<!-- /_new working-->

<body class="signin" style="background:white">
<header class="">
  <div class="container">
    <nav class="navbar navbar-default">       
	<div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="margin-right: -44%;margin-top: 7%;">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>      
    	</div>
      <!-- logo -->
      	
   	   
             	<a href="/" class="logo"><img src="extra/images/logo.png" alt=""></a>
                 <!--<img src="images/bar.png" class="bar" alt="">-->
             
      <!-- /logo -->
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a class="active" href="/"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="contact"> CONTACT</a></li>
                <li><a href="signin">LOGIN</a></li>
                <li><a href="signup">REGISTER</a></li>
        </ul>
      </div>
      </nav>
      <!-- /.navbar-collapse --> 
      
      
</header>

<section>
  
    <div class="signinpanel">
        
        <div class="row">
            
            <div class="col-md-2">
                
               
            </div><!-- col-sm-7 -->
            
            <div class="col-md-8">
                
                <form method="post" action="signup">
                    <h3 class="nomargin">Sign Up</h3>
                    <p class="mt5 mb20">Already have an Account? <a href="<?php echo $baseurl; ?>/signin"><strong>Sign In</strong></a></p>

					
				

<?php

$ref = 0;
if(isset($_GET['ref'])) {
$ref = $_GET['ref'];
}
$rrr = mysql_fetch_array(mysql_query("SELECT id FROM users WHERE username='".$ref."'"));

//echo "$rrr[0]";

if($_POST)
{

$username = $_POST["username"];
$pass1 = $_POST["password1"];
$pass2 = $_POST["password2"];
$email = $_POST["email"];
$country = $_POST["country"];
$phone = $_POST["phone"];


$err1=0;
$err2=0;
$err3=0;
$err4=0;
$err5=0;
$err6=0;
$err7=0;



if(trim($username)=="")
      {
$err1=1;
}

if(trim($email)=="")
      {
$err2=1;
}

if($pass1!=$pass2)
      {
$err3=1;
}

if(strlen($pass1)<="3")
      {
$err4=1;
}

$nnn = mysql_fetch_array(mysql_query("SELECT COUNT(*) FROM users WHERE username='".$username."'"));

if($nnn[0]>="1")
      {
$err5=1;
}

$eee = mysql_fetch_array(mysql_query("SELECT COUNT(*) FROM users WHERE email='".$email."'"));

if($eee[0]>="1")
      {
$err6=1;
}

$ppp = mysql_fetch_array(mysql_query("SELECT COUNT(*) FROM users WHERE phone='".$phone."'"));

if($ppp[0]>="1")
      {
$err7=1;
}




$error = $err1+$err2+$err3+$err4+$err5+$err6+$err7;


if ($error == 0){

$passmd = md5($pass1);

if($rrr[0]=="0" || $rrr[0]==""){
	$r = 1;
}else{
	$r = $rrr[0];
}


$res = mysql_query("INSERT INTO users SET username='".$username."', email='".$email."', password='".$passmd."', phone='".$phone."', country='".$country."', ref='".$r."'");

if($res){
	echo "<div class=\"alert alert-success alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	

Register Completed Successfully!

</div>";

///////////////////------------------------------------->>>>>>>>>Send Email

$to = "$email";
$subject = 'Welcome to SocialMediKo';

$message = 'Hi,'."\r\n Thanks For Joining SocialMediKo";

$headers = 'From: ' . "$sender \r\n" .
    'Reply-To: ' . "$sender \r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);

///////////////////------------------------------------->>>>>>>>>Send Email






}else{
	echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	

Some Problem Occurs, Please Try Again. 

</div>";
}
} else {
	
if ($err1 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	

Username Can Not be Empty!!!

</div>";
}		
	
if ($err2 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	

Email Can Not be Empty!!!

</div>";
}		
	
if ($err3 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	

Passwords Don't match!!

</div>";
}		
	
if ($err4 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	
Password Can not be less than 4 Letter 
</div>";
}		
	
if ($err5 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	
Username Already Exist !
</div>";
}		
	
if ($err6 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	
Email Already Exist !
</div>";
}		
	
if ($err7 == 1){
echo "<div class=\"alert alert-danger alert-dismissable\">
<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>	
Phone Number Already Exist !
</div>";
}		

}







}

?>

                  <input type="text" class="form-control uname" name="username" placeholder="Username" />
                    <input type="text" class="form-control email" name="email" placeholder="Email" />

                    <input type="text" class="form-control phn" name="phone" placeholder="Mobile eg: 8801XXXXXXXXX" />

                   <select class="form-control input-lg"  name="country">

                  <option Value="Bangladesh">Bangladesh</option>
                  <option Value="India">India</option>
                  <option Value="Pakistan">Pakistan</option>
                  <option Value="USA">USA</option>
                  <option Value="Canada">Canada</option>
                  <option Value="UK">UK</option>


                </select>



                    <input type="password" class="form-control pword" name="password1" placeholder="Password" />
                    <input type="password" class="form-control pword" name="password2" placeholder="Retype Password" />

					
                    <button class="btn btn-success btn-block">Sign Up</button>
                    
                </form>
            </div><!-- col-sm-5 -->
            
        </div><!-- row -->
        
      
        
    </div><!-- signin -->
  
</section>

<!-- footer -->
      <footer class="footer">
      	<div class="container">
        <div class="row">
        	<div class="col-lg-3">
            	<h2>About us</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

				<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                
                <h2>Recently Active Memberss</h2>
                <img class="member" src="extra/images/2df1b8205172b7eab5de8d70c90ca6b5.png" alt="">
            
            </div>
            <div class="col-lg-3">
            	<h2>Categories</h2>
                	<ul class="categories">
                    	<li>No categories</li>
                    </ul>
                <h2>Archives</h2>
            </div>
            <div class="col-lg-3">
            	<span>THEMESHAPER</span>
                <div class="text"><a href="#"><b>WHAT WE LOOK FOR IN THEMES FOR WORDPRESS.COM</b></a> September 1, 2016</div>
                <p>What kinds of themes do we look for when we add to our collection on WordPress.com? We get this question a lot, both from existing and potential theme shops. And while some of the specifics evolve over time, the principles of what makes a good theme good remain the same. Whether it’s on WordPress.com or not. […]
</p>
<p><i>-David A. Kennedy</i></p>
            </div>
            <div class="col-lg-3">
            	<h2>Popular Posts</h2>
                
            </div>
        </div>
        </div>
      </footer>
      <script>
	$(document).ready(function(){       
   var scroll_start = 0;
   var startchange = $('.main-menu');
   var offset = startchange.offset();
    if (startchange.length){
   $(document).scroll(function() { 
      scroll_start = $(this).scrollTop();
      if(scroll_start > offset.top) {
           $(".logo img").css('width','72px');
		   $(".main-menu ul").css({'margin-top': '0','margin-bottom':'0'});
       } else {
          $(".main-menu ul").css({'margin-top': '20px','margin-bottom':'20px'});
       		 $(".logo img").css('width','126px');
	   }
   });
    }
});
</script>
<!-- /footer-->
<script src="<?php echo $baseurl; ?>/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo $baseurl; ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo $baseurl; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo $baseurl; ?>/js/modernizr.min.js"></script>
<script src="<?php echo $baseurl; ?>/js/jquery.sparkline.min.js"></script>
<script src="<?php echo $baseurl; ?>/js/jquery.cookies.js"></script>

<script src="<?php echo $baseurl; ?>/js/toggles.min.js"></script>
<script src="<?php echo $baseurl; ?>/js/retina.min.js"></script>

<script src="<?php echo $baseurl; ?>/js/custom.js"></script>
<script>
    jQuery(document).ready(function(){
        
        // Please do not use the code below
        // This is for demo purposes only
        var c = jQuery.cookie('change-skin');
        if (c && c == 'greyjoy') {
            jQuery('.btn-success').addClass('btn-orange').removeClass('btn-success');
        } else if(c && c == 'dodgerblue') {
            jQuery('.btn-success').addClass('btn-primary').removeClass('btn-success');
        } else if (c && c == 'katniss') {
            jQuery('.btn-success').addClass('btn-primary').removeClass('btn-success');
        }
    });
</script>

</body>
</html>



