<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="//media.gadventures.com/media-server/static/_CACHE/css/93283ea14fb6.css" type="text/css">
  <link rel="stylesheet" href="//media.gadventures.com/media-server/static/_CACHE/css/411a4fe95313.css" type="text/css">
  <link rel="stylesheet" href="//media.gadventures.com/media-server/static/_CACHE/css/69a3055be063.css" type="text/css">
</head>
<body>
<?php
error_reporting(0);
 $context = stream_context_create(
  array(
     'http' => array(
         'header' => "X-Application-Key: live_bab27dd6dc7e535db43b1d6896e8514a48f54a27"
       ),
      'ssl'  =>array(
          "verify_peer"=>false,
          "verify_peer_name"=>false,
      ),
    ));
  $url = 'https://rest.gadventures.com/tours';
  $response = file_get_contents($url, false, $context);

  $res = json_decode($response);

  $result = $res->results;
  $id_array = array();
  foreach($result as $value){
      $id = $value->id;
      $tour_dossier = $value->tour_dossier->href;
      $id_array[$id] = $tour_dossier;
  }

  // echo '<pre>';

  // print_r($id_array);

  $detail_array = array();
  foreach ($id_array as $key => $value) {
    # code...
    $de = file_get_contents($value, false, $context);

    $response_detail = json_decode($de);

    //print_r($response_detail);

    $detail = array();

    $detail['id'] = $response_detail->id;
    $detail['product_line'] = $response_detail->product_line;
    $detail['tour_name'] = $response_detail->name;
    $detail['days'] = $response_detail->itineraries[0]->duration;
    $detail['map'] = $response_detail->images[0]->image_href;
    $detail['region'] = $response_detail->geography->region->name;
    foreach ($response_detail->categories as $key2 => $value2) {
      # code...
      if($value2->category_type != null && $value2->category_type->label == 'Service Level'){
          $detail['service_level'] = $value2->name;
      }
      if($value2->category_type != null && $value2->category_type->label == 'Trip Type'){
          $detail['tour_type'] = $value2->name;
      }

    }
    $detail['price'] = $response_detail->advertised_departures[0]->amount;
    $detail['primary_country'] = $response_detail->geography->primary_country->name;

    $detail_array[] = $detail;

  }

foreach ($detail_array as $key1 => $data_value) {
  # code...


?>

<div class="faceted-tile"><div class="trip-tile-header yolo"></div><div class="trip-tile-image show-map"><a href="detail.php" id="map<?php echo $data_value['product_line']; ?>" class="trip-tile-map t-url"><img alt="Map of <?php echo $data_value['tour_name']; ?>" width="318" height="318" src="<?php echo $data_value['map']; ?>"></a><a href="/trips/indochina-discovery/2689/" id="banner<?php echo $data_value['tour_name']; ?>" class="trip-tile-banner t-url"><img alt="" data-src="<?php echo $data_value['map']; ?>" width="318" height="318" src="//media.gadventures.com/media-server/static/faceted_trips/images/map-loading.png"></a></div><div class="trip-panel" id="<?php echo $data_value['product_line']; ?>">

<div class="trip-info"><div class="trip-name"><a href="/trips/indochina-discovery/2689/" class="t-url"><?php echo $data_value['tour_name']; ?></a></div><div class="trip-details"><div class="trip-priceduration clearfix"><div class="pricing"><div class="price-info"><span class="from">From</span><span class="price"><span title="USD" class="USD">$</span><strong class="pullPrice"><?php echo $data_value['price']; ?></strong></span></div></div><div class="duration">
                            <?php echo $data_value['days']; ?>
                            <small>days</small></div></div><div class="trip-attrs clearfix"><span>
                      Type
                      <a href="" ceid="Trip Type" data-meta-field="trip_type" data-meta-value="<?php echo $data_value['tour_type']; ?>" title="" data-original-title="Type - <?php echo $data_value['tour_type']; ?>" style="cursor: pointer;"> <?php echo $data_value['tour_type']; ?></a></span><span>
                      Service Level
                      <a href="#" ceid="Service Level" data-meta-field="service_level" data-meta-value="<?php echo $data_value['service_level']; ?>" title="" data-original-title="Service Level - <?php echo $data_value['service_level']; ?>" style="cursor: pointer;"> <?php echo $data_value['service_level']; ?></a></span><span>
                      Physical Grading
                      <a href="#" ceid="Physical Grading"><img alt="Physical grading level Light" src="//media.gadventures.com/media-server/static/trip_summary/css/images/phys_rate_Light.png" title="" data-meta-field="physical_grading" data-meta-value="Light" data-original-title="Physical Grading - Light" style="cursor: pointer;"></a></span></div></div></div><div class="trip-tile-footer"><span class="trip-code"><?php echo $data_value['product_line']; ?></span><a href="detail.php" class="btn btn-mini btn-goal trip-cta t-url" ceid="ViewTripCTA_1">View Trip</a></div></div></div>



 
<?php } ?>
</body>
</html>