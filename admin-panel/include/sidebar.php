<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

  <div class="leftpanel">

    <div class="logopanel">
        <h1>SocialMediaKo ADMIN Panel</h1>
    </div><!-- logopanel -->

    <div class="leftpanelinner">

        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">

            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">

              <li><a href="signout.php"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>

      <h5 class="sidebartitle">Navigation</h5>
      <ul class="nav nav-pills nav-stacked nav-bracket">
       
<li><a href="home.php"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
<li><a href="setgeneral.php"><i class="fa fa-cog"></i> General Setting</a></li>

<li><a href="addservice.php"><i class="fa fa-edit"></i> Add New Service</a></li>
<li><a href="listservice.php"><i class="glyphicon glyphicon-tasks"></i> Service List</a></li>


<li><a href="listorder.php"><i class="fa fa-bullhorn"></i> Order Management</a></li>

<li><a href="addbalance.php"><i class="fa fa-dollar"></i> Add Balance Req</a></li>



		

      </ul>

      

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->

  <div class="mainpanel">

    <div class="headerbar">

      <a class="menutoggle"><i class="fa fa-bars"></i></a>


      <div class="header-right">
        <ul class="headermenu">
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <img src="images/photos/loggeduser.png" alt="" />
<?php
echo " $user";
?>
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
      <li><a href="changepass.php"><i class="fa fa-sign-out"></i> <span>Change Password</span></a></li>
              <li><a href="signout.php"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
              </ul>
            </div>
          </li>

        </ul>
      </div><!-- header-right -->

    </div><!-- headerbar -->
